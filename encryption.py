from Crypto.PublicKey import RSA
from Crypto import Random
import ast


def randomSeedGen():  # generate seed to use in private key generation
    seed = Random.new().read
    return seed


def privateKeyGen(seed):  # generate private key
    key = RSA.generate(1024, seed)
    return key


def publicKeyGen(privateKey):  # generate public key for a private key
    publicKey = privateKey.publickey()
    return publicKey


def saveKey(filename, key):
    savekey = open(filename, 'wb')  # write public key to file in PEM format
    savekey.write(key.exportKey('PEM'))
    savekey.close()


def encryptMessage(plaintext, publicKey):
    encrypted = publicKey.encrypt(plaintext, 32)
    return encrypted


def saveMessage(filename, message):
    m = open(filename, 'wb')
    messageString = str(message)
    m.write(messageString)  # write ciphertext to file
    m.close()


def openEncryptedMessage(filename):
    e = open(filename, 'rb')  # open ciphertext
    ciphertext = e.read()
    e.close()
    return ciphertext


def decryptMessage(ciphertext, privateKey):
    ciphertextString = str(ciphertext)
    evaluate = ast.literal_eval(ciphertextString)
    decrypted = privateKey.decrypt(evaluate)  # decrypt ciphertext
    return decrypted


def loadKey(filename):
    keypath = open(filename, 'rb')  # path to private key file
    readkey = keypath.read()
    key = RSA.importKey(readkey)  # import private key
    keypath.close()
    return key


if __name__ == '__main__':
    seed = randomSeedGen()  # generate seed to use in private key generation
    privateKey = privateKeyGen(seed)  # generate private key

    publicKey = publicKeyGen(privateKey)  # generate public key for private key

    savePublicKeyFilename = raw_input('enter public key name: ')
    saveKey(savePublicKeyFilename, publicKey)  # write public key to file in PEM format

    savePrivateKeyFilename = raw_input('enter private key name: ')
    saveKey(savePrivateKeyFilename, privateKey)  # write private key to file in PEM format
