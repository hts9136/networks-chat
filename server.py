import socket
import select
import threading
import os
import encryption
import symmetric

_HOST = '0.0.0.0'  # 0.0.0.0 is localhost
_PORT = 5001  # port to listen on


class ChatServer(threading.Thread):
    MAX_WAITING_CONNECTIONS = 10
    RECV_BUFFER = 4096

    def __init__(self, host, port):
        threading.Thread.__init__(self)
        self.host = host
        self.port = port
        self.connections = []  # collects all the incoming connections
        self.running = True  # tells whether the server should run

    def checkAES(self, sock):  # determines if aes key is present before sending
        filename = 'symkey.pem'
        if os.path.isfile(filename):  # determines if aes key file is present
            self.sendKey(sock, filename)
        else:
            print '!!!ERROR AES key not found!!!'
            print '!!!GENERATING NEW AES KEY!!!'
            symmetric.AESkeygenSave(filename)  # generate new symmetric key and save it to file
            print 'AES key saved as symkey.pem'
            self.sendKey(sock, filename)

    def sendKey(self, sock, filename):  # serves aes key to client
        print 'AES key file found'
        print 'Sending aeskey...'
        sock.send('EXISTS ' + str(os.path.getsize(filename)))  # tells client to prepare for key exchange
        userResponse = sock.recv(1024)

        if userResponse[:2] == 'OK':  # starts key exchange when client says it is ready and sends client public key
            f = open('keytouse', 'wb')
            data = sock.recv(1024)
            f.write(data)  # write client public key to file
            f.close()
        keyToUse = encryption.loadKey('keytouse')  # load client public key from file
        loadedAES = symmetric.loadAESkey(filename)  # load aes key from file
        encryptedAES = symmetric.encryptAESkey(keyToUse, loadedAES)  # encrypt aes key with client public key
        symmetric.saveAESkey('packedkey', encryptedAES)  # save encrypted aes key to file
        with open('packedkey', 'rb')as f:  # load encrypted aes key from file
            bytesToSend = f.read(1024)  # read bytes from encrypted aes key
            sock.send(bytesToSend)  # send encrypted aes key bites
            print 'AES key sent'

    def _bind_socket(self):  # Creates the server socket and binds it to the given host and port.

        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.bind((self.host, self.port))
        self.server_socket.listen(self.MAX_WAITING_CONNECTIONS)
        self.connections.append(self.server_socket)

    def _send(self, sock, msg):
        sock.send(msg)

    def _receive(self, sock):
        data = sock.recv(1024)
        return data

    def _broadcast(self, client_socket, client_message):  # Broadcasts message to all clients in client list

        for sock in self.connections:
            is_not_the_server = sock != self.server_socket
            is_not_the_client_sending = sock != client_socket
            if is_not_the_server and is_not_the_client_sending:
                try:
                    self._send(sock, client_message)
                except socket.error:
                    sock.close()
                    self.connections.remove(sock)  # removing the socket from the active connections list

    def _run(self):  # implements server logic functions

        while self.running:
            # Gets the list of sockets which are ready to be read through select non-blocking calls
            # The select has a timeout of 60 seconds
            try:
                ready_to_read, ready_to_write, in_error = select.select(self.connections, [], [], 60)
            except socket.error:
                continue
            else:
                for sock in ready_to_read:

                    if sock == self.server_socket:  # If the socket instance is the server socket
                        try:
                            # Handles a new client connection
                            client_socket, client_address = self.server_socket.accept()
                        except socket.error:
                            break
                        else:
                            self.connections.append(client_socket)
                            print "Client (%s, %s) connected" % client_address

                    else:  # else socket instance is an incoming client socket connection
                        try:
                            data = self._receive(sock)  # Gets the client message
                            if data:
                                if data == "::KEY::":
                                    self.checkAES(sock)
                                else:
                                    print str(data)
                                    self._broadcast(sock,
                                                    data)  # broadcasts client message to all the connected clients
                        except socket.error:
                            print "Client (%s, %s) is offline" % client_address
                            sock.close()
                            self.connections.remove(sock)
                            continue

        self.stop()  # Clears the socket connection

    def run(self):  # Given a host and a port, binds the socket and runs the server.
        self._bind_socket()
        self._run()


if __name__ == '__main__':
    chat_server = ChatServer(_HOST, _PORT)
    chat_server.start()
