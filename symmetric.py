import base64
from Crypto.Cipher import AES
from Crypto.PublicKey import RSA
import ast
import os


# AES key == symmetric key

def pad(s,
        block_size=16):  # pad the text to be encrypted; the length of encrypted message must always be a multiple of block_size
    padding = '{'  # character used for padding
    return s + (block_size - len(s) % block_size) * padding


def EncodeAES(cipher, string):  # encrypt with AES, encode with base64
    return base64.b64encode(cipher.encrypt(pad(string)))


def DecodeAES(cipher, encoded):
    padding = '{'  # character used for padding
    return cipher.decrypt(base64.b64decode(encoded)).rstrip(padding)


def AESkeygen(block_size=16):  # generate symmetric key
    return os.urandom(block_size)


def exportAESkey(filename, key):  # save symmetric key to file
    saveaeskey = open(filename, 'wb')
    saveaeskey.write(key)
    saveaeskey.close()


def loadAESkey(filename):  # load symmetric key from file
    readsym = open(filename, 'rb')
    loadedAESkey = readsym.read()
    readsym.close()
    return loadedAESkey


def loadRSAKey(filename):  # load private key from file
    rsaKeyPath = open(filename, 'rb')  # path to private key file
    rsaKey = RSA.importKey(rsaKeyPath.read())  # import private key
    rsaKeyPath.close()
    return rsaKey


def encryptAESkey(publicKey, AESkey):  # encrypt symmetric key with public key
    return publicKey.encrypt(AESkey, 32)


def saveAESkey(filename, aesKey):
    saveAESkeyPath = open(filename, 'wb')  # save encrypted symmetric key
    saveAESkeyPath.write(str(aesKey))
    saveAESkeyPath.close()


def loadAESkey(filename):
    aesKeypath = open(filename, 'rb')  # load encrypted symmetric key
    aesKey = aesKeypath.read()
    aesKeypath.close()
    return aesKey


def decryptAESkey(privatekey, aesKey):
    parseAES = ast.literal_eval(aesKey)
    return privatekey.decrypt(parseAES)  # decrypt symmetric key with private key


def createCipher(key):
    cipher = AES.new(key)
    return cipher


def AESkeygenSave(filename):  # generate a new symmetric key AND save it to file
    key = AESkeygen()
    exportAESkey(filename, key)


if __name__ == '__main__':
    newAESkeyFilename = 'symkey.pem'  # filename to save new symmetric key to
    AESkeygenSave(newAESkeyFilename)  # generate new symmetric key and save it to file
