import socket
import threading
import symmetric
import encryption
import os


class Client:

    def __init__(self, address, port):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.username = raw_input("enter username: ") + ": "
        try:
            self.loadedAESkey = symmetric.loadAESkey('symkey.pem')

        except:
            print '!!!WARNING: AES key file not found!!!'
            print 'PERFORMING KEY EXCHANGE...'
            self.requestKey(address, port)
            self.loadedAESkey = symmetric.loadAESkey('symkey.pem')

        self.cipher = symmetric.createCipher(self.loadedAESkey)
        self.encryptedConnection(address, port)

    def encryptedConnection(self, address, port):
        self.sock.connect((address, port))

        iThread = threading.Thread(target=self.sendMsg)
        iThread.daemon = True
        iThread.start()

        while True:
            data = self.sock.recv(1024)
            if not data:
                break
            else:
                decodedData = symmetric.DecodeAES(self.cipher, data)
                print(decodedData)

    def requestKey(self, address, port):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((address, port))

        filename = 'encryptedsym'
        s.send('::KEY::')
        data = s.recv(1024)
        if data[:6] == 'EXISTS':
            message = raw_input('AES key ready, Download? (Y/N)? -> ')

            if message == 'Y' or 'y':
                s.send('OK')
                if os.path.isfile('publickey'):
                    self.keyExchange(s, filename)

                else:
                    print '!!!RSA key pair not found!!!'
                    print '!!!Generating new RSA key pair'
                    seed = encryption.randomSeedGen()  # generate seed to use in private key generation
                    privateKey = encryption.privateKeyGen(seed)  # generate private key

                    publicKey = encryption.publicKeyGen(privateKey)  # generate public key for private key

                    savePublicKeyFilename = 'publickey'
                    encryption.saveKey(savePublicKeyFilename, publicKey)  # write public key to file in PEM format

                    savePrivateKeyFilename = 'privatekey'

                    encryption.saveKey(savePrivateKeyFilename, privateKey)  # write private key to file in PEM format
                    print 'RSA publickey and privatekey saved'
                    self.keyExchange(s, filename)
        else:
            print 'File does not exist!'

        s.close()

    def keyExchange(self, s, filename):
        with open('publickey', 'rb')as f:
            bytesToSend = f.read(1024)
            s.send(bytesToSend)
            f.close()

        f = open(filename, 'wb')
        data = s.recv(1024)
        f.write(data)

        f.close()

        encryptedSym = symmetric.loadAESkey('encryptedsym')
        privKey = encryption.loadKey('privatekey')
        decryptedSym = symmetric.decryptAESkey(privKey, encryptedSym)
        symmetric.exportAESkey('symkey.pem', decryptedSym)
        print 'Download Complete! AES key saved as symkey.pem'

    def sendMsg(self):
        while True:
            message = raw_input('->')
            encodeString = self.username + message
            encoded = symmetric.EncodeAES(self.cipher, encodeString)
            self.sock.send(encoded)


if __name__ == '__main__':
    address = '127.0.0.1'  # 127.0.0.1 is just the loopback address for servers and clients running on same machine
    port = 5001
    Client(address, port)
